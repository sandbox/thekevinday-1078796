CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This is a multi-purpose project designed to assist in making and using any
given node type easier than what is done in drupal core. The primary purpose
is to overhaul the form build system in such a way that it is more
user-friendly than the drupal core approach. This module can co-exist with
the drupal core functionality.

This provides sub-modules that assist in the management and use of node types
as well as the management and use of nodes. Read the readme.txt for each
individual sub-module for further information on what each sub-module does.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>

