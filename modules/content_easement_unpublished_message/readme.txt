CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This provides a simple message on the screen when a particular node the current
user is viewing is unpublished. This helps make it more obvious to the user
that a particular node is unpublished in a theme independent way. This is
particular useful when combined with another module that allows users to view
unpublished content that they do not own.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>
