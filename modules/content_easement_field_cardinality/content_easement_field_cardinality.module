<?php

/**
 * Implements hook_form_alter() for field_ui_field_edit_form.
 */
function content_easement_field_cardinality_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  $function_history = array();
  cf_error_append_history($function_history, __FUNCTION__);

  if (!is_array($form)){
    cf_error_invalid_array($function_history, 'form');
    return;
  }

  // Having a select list with only 10 fields is a problem.
  // This removes the idea of having only options of 1->10 or infinite to the extend that any number may be used.
  $form['field']['cardinality']['#type'] = 'textfield';
  $form['field']['cardinality']['#description'] = t("Maximum number of values users can enter for this field.") . "<br>\n" . t("To have an UNLIMITED amount of values, enter in the value: %unlimited_cardinality.", array('%unlimited_cardinality' => FIELD_CARDINALITY_UNLIMITED));
  unset($form['field']['cardinality']['#options']);

  if (!is_array($form['#validate'])){
    $form['#validate'] = array();
  }

  $form['#validate'][] = 'content_easement_field_cardinality_validate';
}

/**
 * When the cardinality was changed, the possible options had to be removed.
 * This, however, allows for non-numeric values to be inserted and therefore additional validation is needed.
 */
function content_easement_field_cardinality_validate($form, &$form_state){
  $function_history = array();
  cf_error_append_history($function_history, __FUNCTION__);

  if (function_exists('cf_version_exists')) {
    if (cf_is_not_form_state('form_state', $form_state)){
      return;
    }
  }
  else {
    if (cf_is_not_form_state($function_history, 'form_state', $form_state)){
      return;
    }
  }

  if (isset($form_state['values']['field']['cardinality'])){
    if (!is_numeric($form_state['values']['field']['cardinality']) || ($form_state['values']['field']['cardinality'] != FIELD_CARDINALITY_UNLIMITED && $form_state['values']['field']['cardinality'] < 0)){
      form_set_error('field][cardinality', t("The Number of Values must be either %unlimited_cardinality for an unlimited number of values or a positive number.", array('%unlimited_cardinality' => FIELD_CARDINALITY_UNLIMITED)));
    }
  }
}
