CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This provides the ability to manually define what the cardinality for a given
field should be. By default drupal only provides users with a small selection
of cardinality choices, ranging from 1 to 10. This is unreasonable as it is
very much possible that a user may need to have 11, 12, or 10000 fields.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>


