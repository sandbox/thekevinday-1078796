CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This alters how the field user interface is provided to users. The primary
purpose of this is to help automate some of the steps used to manage a given
node type. The secondary purpose is to provide a user interface that works
well with the category functionality introduced by the content easement
category module.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>

