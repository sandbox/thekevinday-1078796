CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This provides classification and categorization for different node types.
With this module node types can be managed only by those users who are
allowed to manage node types for a given category. Fields can only be
between node types of the same category.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>

