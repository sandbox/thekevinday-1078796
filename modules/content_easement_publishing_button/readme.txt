CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Contributers


INTRODUCTION
------------

This provides an unpublish/publish button for convenience and user-friendliness
purposes. Provides additional access control such that non-admins can see,
and therefore use, the publish and unpublished buttons.


CONTRIBUTERS
------------
Kevin Day <thekevinday@gmail.com>
